using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMusic : MonoBehaviour
{
    public AudioSource _as;
    //list of all the music
    public AudioClip[] audioClipArray;

    void Awake()
    {
        _as = GetComponent<AudioSource> ();
    }

    // Start is called before the first frame update
    void Start()
    {
        _as = GetComponent<AudioSource>();
        _as.clip = audioClipArray[Random.Range(0,audioClipArray.Length)];
        _as.PlayOneShot(_as.clip);
    }
}
