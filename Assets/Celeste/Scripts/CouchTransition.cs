using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CouchTransition : MonoBehaviour
{
    public Material mat;
    private float lerpvalue = 0f;
    [SerializeField]
    float smoothness;
    float length = 10;


    private void Start()
    {
        mat.SetFloat("_Blend", 0);
    }


    // Update is called once per frame
    private void Update()
    {
        StartCoroutine(ChangeTexture(length));

    }
    IEnumerator ChangeTexture(float duration)
    {
        float time = 0;


        while (time < duration)
        {
            lerpvalue = Mathf.Lerp(lerpvalue, 1f, smoothness * Time.deltaTime);
            mat.SetFloat("_Blend", lerpvalue);
            time += Time.deltaTime;
            yield return null;
        }



    }
}
