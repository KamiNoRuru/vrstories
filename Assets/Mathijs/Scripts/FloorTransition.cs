using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTransition : MonoBehaviour
{
    public Material mat;
    public GameObject grass;
    private float lerpvalue = 0f;
    [SerializeField]
    float smoothness;
    float length = 10;
    float startHeight = -0.1f;


    private void Start()
    {
        mat.SetFloat("_Blend", 0);
        grass.transform.position = new Vector3(grass.transform.position.x, startHeight, grass.transform.position.z);
    }


    // Update is called once per frame
    private void Update()
    {
        StartCoroutine(ChangeTexture(length));
        if(mat.GetFloat("_Blend")> 0.5f)
        {
            StartCoroutine(GrowGrass(length));
        }
        
    }
    IEnumerator ChangeTexture(float duration)
    {
        float time = 0;
     

        while(time < duration)
        {
            lerpvalue = Mathf.Lerp(lerpvalue, 1f, smoothness * Time.deltaTime);
            mat.SetFloat("_Blend", lerpvalue);
            time += Time.deltaTime;
            yield return null;
        }
      


    }
    IEnumerator GrowGrass(float duration)
    {
        float goalHeight = 0.81f;
        float time = 0;



        while (time < duration)
        {
            startHeight = Mathf.Lerp(startHeight, goalHeight, smoothness * 10 * Time.deltaTime);
            grass.transform.position = new Vector3(grass.transform.position.x, startHeight, grass.transform.position.z);
            time += Time.deltaTime;
            yield return null;
        }
    }
}
