using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndingTimer : MonoBehaviour
{
    float timer = 0f;
    int  minutes = 4;
    public GameObject birdLamp;
    public Text text;
    private float fade = 0.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if(timer >= (minutes * 60))
        {
            birdLamp.SetActive(true);
        }
        if (timer >= (minutes * 60 - ((1 / 5) * minutes * 60)))
        {
            fade += 0.5f*Time.deltaTime;

            Color color = text.color;
            color.a = fade;
            text.color = color;
        }

    }
}
