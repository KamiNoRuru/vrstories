using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour
{
    private float mSize = 0.0f;
    public Animator _animator;
    private bool _ended = false;
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponentInParent<Animator>();
    }

    // Update is called once per frame
    void Scale()
    {
        if(mSize >= 100.0f)
        {
            CancelInvoke("Scale");
        }
        GetComponent<SkinnedMeshRenderer>().SetBlendShapeWeight(0, mSize++);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "BirdEnd")
        {
            InvokeRepeating("Scale", 0.0f, 0.01f);
            _ended = true;
        }
    }
    private void Update()
    {
        if (_ended)
        {
            if (_animator.GetCurrentAnimatorStateInfo(0).IsName("FlyUp") && _animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
            {
                _animator.SetBool("Ended", true);
            }
        }
    }
}
