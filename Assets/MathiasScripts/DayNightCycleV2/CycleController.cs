using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CycleController : MonoBehaviour
{
    [SerializeField] private Light directionalLight;
    [SerializeField] private Light moon;
    //[SerializeField, Range(0, 25)] private float timeOfDay;

    public float deg;

    public float DayTimeLength;
    public float NightTimeLength;
    public float CalculatedRotationValue;
    public float smoothnessFactor;

    private float LerpValue = 1;

    private ParticleSystem ps;
    public List<ParticleSystem> Fireflies;

    public bool isDayTime;
    public bool isNightTime;
    public bool lerpDaytime = false;
    public bool lerpNighttime = false;

    float daySkyColor = 1;
    float nightSkyColor = 5;

    public int timeBeforeDawn;
    public int timeBeforeDusk;

    public Material skybox;
    public Material nightSkyBox;

    private void OnValidate()
    {
        RenderSettings.skybox.SetFloat("_AtmosphereThickness", 1);
        if (directionalLight != null)
            return;

        if (RenderSettings.sun != null)
        {
            directionalLight = RenderSettings.sun;
        }
        else
        {
            Light[] lights = GameObject.FindObjectsOfType<Light>();
            foreach (Light light in lights)
            {
                if (light.type == LightType.Directional)
                {
                    directionalLight = light;
                    return;
                }
            }
        }
    }


    // private void UpdateLighting(float timepercent)
    // {
    //     directionalLight.gameObject.transform.localEulerAngles = new Vector3(timeOfDay * 7.5f, 0f, 0f);       
    // }
    public static float Clamp0360(float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if (result < 0)
        {
            result += 360f;
        }
        return result;
    }
    private void Update()
    {
        CalculatedRotationValue = Clamp0360(directionalLight.transform.localEulerAngles.x);
        //Grabs the rotation of the Directional Light. These rotations are the period of time that there is light, creating the Day cycle
        if (directionalLight.transform.localEulerAngles.x > 0 && directionalLight.transform.localEulerAngles.x < 180)
        {
            isNightTime = false;
            isDayTime = true;

        }
        //Grabs the rotation of the Directional Light. These rotations are the period of time that there is darkness, creating the Night cycle
        else if (directionalLight.transform.localEulerAngles.x > 180 && directionalLight.transform.localEulerAngles.x < 360)
        {
            isDayTime = false;
            isNightTime = true;
        }

        /*if (Mathf.Floor(transform.localRotation.x) == 360 && isDayTime)
        {
            StartCoroutine(LerpDayToNight(daySkyColor, NightSkyColor, 3));
        }
        else if (Mathf.Floor(transform.localRotation.x) == 180 && isNightTime)
        {
            StartCoroutine(LerpDayToNight(NightSkyColor, daySkyColor, 3));
        }*/

        //DON'T USE 2 RANDOM FLOATS, CHANGE 1 SKYCOLOR THIS IS THE ERROR



        //begins lerping skycolor based on bool handled in lerptimer
        if (lerpDaytime)
        {
            StartCoroutine(LerpDayToNight(3));
        }
        if (lerpNighttime)
        {
            StartCoroutine(LerpNightToDay(3));
        }

        #region DayNight rotation and period specifics
        directionalLight.transform.Rotate(1 * DayTimeLength * Time.deltaTime, 0, 0);
        moon.transform.Rotate(1 * NightTimeLength * Time.deltaTime, 0, 0);

        //rotates sun TODO: rotate moon
        if (isDayTime)
        {
            directionalLight.gameObject.SetActive(true);
            moon.gameObject.SetActive(false);
            RenderSettings.skybox.SetFloat("_AthmospheerThickness", 4.999533f);
            RenderSettings.skybox = skybox;
            RenderSettings.sun = directionalLight;

            if (CalculatedRotationValue >= Mathf.Floor(0))
            {
                foreach (ParticleSystem fireflies in Fireflies)
                {
                    ps = fireflies.GetComponent<ParticleSystem>();
                    var main = ps.main;
                    main.loop = false;
                }
                StartCoroutine(LerpTimerDay(DayTimeLength));
            }
        }

        if (isNightTime)
        {
            directionalLight.gameObject.SetActive(false);
            moon.gameObject.SetActive(true);
            RenderSettings.skybox.SetFloat("_AthmospheerThickness", 4.999533f);
            RenderSettings.skybox = nightSkyBox;
            RenderSettings.sun = moon;

            if (CalculatedRotationValue >= Mathf.Floor(0))
            {
                foreach (ParticleSystem fireflies in Fireflies)
                {
                    ps = fireflies.GetComponent<ParticleSystem>();
                    ps.gameObject.SetActive(true);
                    var main = ps.main;
                    main.loop = true;
                }
                StartCoroutine(LerpTimerNight(NightTimeLength));
            }
        }
        #endregion

        //OLD VERSION
        // if (Application.isPlaying)
        // {
        //     timeOfDay += Time.deltaTime;
        //     timeOfDay %= 24;
        //     UpdateLighting(timeOfDay / 24f);
        // }
        // else
        // {
        //     UpdateLighting(timeOfDay / 24f);
        // }
        //  foreach (ParticleSystem fireflies in Fireflies)
        //  {
        //      ps = fireflies.GetComponent<ParticleSystem>();
        //      var main = ps.main;
        //      main.loop = false;
        //  }
    }


    IEnumerator LerpDayToNight(float duration)
    {
        float time = 0;

        while (time < duration)
        {
            LerpValue = Mathf.Lerp(LerpValue, nightSkyColor, smoothnessFactor * Time.deltaTime);
            RenderSettings.skybox.SetFloat("_AtmosphereThickness", LerpValue);
            time += Time.deltaTime;
            yield return null;
        }
        //RenderSettings.skybox.SetFloat("_AtmosphereThickness", endValue);
    }
    IEnumerator LerpNightToDay(float duration)
    {
        float time = 0;

        while (time < duration)
        {
            LerpValue = Mathf.Lerp(LerpValue, daySkyColor, smoothnessFactor * Time.deltaTime);
            RenderSettings.skybox.SetFloat("_AtmosphereThickness", LerpValue);
            time += Time.deltaTime;
            yield return null;
        }
        //RenderSettings.skybox.SetFloat("_AtmosphereThickness", endValue);
    }
    IEnumerator LerpTimerDay(float dayTimeLength)
    {
        yield return new WaitForSeconds((180 / dayTimeLength) - timeBeforeDawn);
        if (lerpDaytime == false)
        {
            lerpDaytime = true;
            lerpNighttime = false;
        }
    }
    IEnumerator LerpTimerNight(float nightTimeLength)
    {
        yield return new WaitForSeconds((180 / nightTimeLength) - timeBeforeDusk);
        if (lerpNighttime == false)
        {
            lerpNighttime = true;
            lerpDaytime = false;
        }

    }
}
