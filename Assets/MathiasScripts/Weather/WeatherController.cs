using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System;
using System.IO;
using Assets;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

public class WeatherController : MonoBehaviour
{

    private const string API_KEY = "4cfc94adbd4357b53febaa83ccf08810";
    public string CityId;

    public GameObject Snow;
    public GameObject Rain;

    private const float API_CHECK_MAXTIME = 10 * 60.0f; //10 minutes   
    private float _apiCheckCountdown = API_CHECK_MAXTIME;

    [SerializeField]
    private string lat = "52.66";
    [SerializeField]
    private string lng = "4.78333";

    void Start()
    {
        CheckSnowStatus();
        CheckRainStatus();
    }
    private void Update()
    {
        _apiCheckCountdown -= Time.deltaTime;
        if (_apiCheckCountdown <= 0)
        {
            CheckSnowStatus();
            CheckRainStatus();
            _apiCheckCountdown = API_CHECK_MAXTIME;
        }
    }

    public async void CheckSnowStatus()
    {
        bool snowing = (await GetWeather()).weather[0].main.Equals("Snow");
        
        if (snowing)
            Snow.SetActive(true);
        else
            Snow.SetActive(false);
    }
    public async void CheckRainStatus()
    {
        bool raining = (await GetWeather()).weather[0].main.Equals("Rain");

        if (raining)
            Rain.SetActive(true);
        else
            Rain.SetActive(false);
    }
    private async Task<WeatherInfo> GetWeather()
    {
        // HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("http://api.openweathermap.org/data/2.5/weather?id={0}&APPID={1}", CityId, API_KEY));
        EventManager eventManager = gameObject.GetComponent<EventManager>();
        lat = eventManager.lat;
        lng = eventManager.lng;

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("http://api.openweathermap.org/data/2.5/weather?lat={0}&lon={1}&appid={2}", lat, lng, API_KEY));
        HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync());
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();
        WeatherInfo info = JsonUtility.FromJson<WeatherInfo>(jsonResponse);

        return info;
        
    }

}
