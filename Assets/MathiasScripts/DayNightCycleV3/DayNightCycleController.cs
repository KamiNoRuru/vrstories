using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightCycleController : MonoBehaviour
{
    [SerializeField] private Light directionalLight;
    [SerializeField] private Light moon;
    public float deg;

    public float DayTimeLength;
    public float NightTimeLength;
    public float CalculatedRotationValue;
    public float smoothnessFactor;
    public float speedFactor;
    float intensity = 0;
    //LerpValues
    private float LerpValue = 1;
    private float sunIntensityValue = 1;
    private Color skyColor;
    private Color groundColor;
    private float sunSize;
    private float sunConvergence;
    private float exposure;

    private Color daySky;
    private Color nightSky;
    private Color dayGround;
    private Color nightGround;

    private ParticleSystem ps;
    public List<ParticleSystem> Fireflies;
    public List<Light> lights;
    public List<Light> NightLights;
    public ParticleSystem stars;

    public bool isDayTime;
    public bool isNightTime;
    public bool halfWayDay = false;
    public bool halfWayNight = false;
    public bool lerpDaytime = false;
    public bool lerpNighttime = false;

    float daySkyColor = 1;
    float nightSkyColor = 5;

    public int timeBeforeDawn;
    public int timeBeforeDusk;

    public Material skybox;
    public Material nightSkyBox;

    private float daySkyR = 0.0f;
    private float daySkyG = 17.0f;
    private float daySkyB = 15.0f;

    private float dayGroundR = 115;
    private float dayGroundG = 120;
    private float dayGroundB = 120;

    private float nightSkyR = 139;
    private float nightSkyG = 171;
    private float nightSkyB = 0;

    private float nightGroundR = 180;
    private float nightGroundG = 38;
    private float nightGroundB = 102;

    private float daySunSize = 0.072f;
    private float nightSunSize = 0.065f;

    private float daySunConvergence = 5f;
    private float nightSunConvergence = 8.5f;

    private float dayExposure = 1.2f;
    private float nightExposure = 0.15f;

    private float moonIntensity = 0.35f;
    private float sunIntensity = 1f;

    private IEnumerator dayToNightSkyColor;
    private IEnumerator nightToDaySkyColor;

    private bool stopDayToNightSkyColor;
    private bool stopNightToDaySkyColor;

    private void OnValidate()
    {
        daySky = new Color(daySkyR / 255, daySkyG / 255, daySkyB / 255);
        dayGround = new Color(dayGroundR / 255, dayGroundG / 255, dayGroundB / 255);

        nightSky = new Color(nightSkyR / 255, nightSkyG / 255, nightSkyB / 255);
        nightGround = new Color(nightGroundR / 255, nightGroundG / 255, nightGroundB / 255);

        skyColor = daySky;
        groundColor = dayGround;
        sunSize = daySunSize;
        sunConvergence = daySunConvergence;
        exposure = dayExposure;

        RenderSettings.skybox.SetColor("_SkyTint", daySky);
        RenderSettings.skybox.SetFloat("_AtmosphereThickness", 1);
        RenderSettings.skybox.SetFloat("_SunSize", daySunSize);
        RenderSettings.skybox.SetFloat("_SunSizeConvergence", daySunConvergence);
        RenderSettings.skybox.SetFloat("_Exposure", dayExposure);

        ResetCoroutines();

        if (directionalLight != null)
            return;

        if (RenderSettings.sun != null)
        {
            directionalLight = RenderSettings.sun;
        }
        else
        {
            Light[] lights = GameObject.FindObjectsOfType<Light>();
            foreach (Light light in lights)
            {
                if (light.type == LightType.Directional)
                {
                    directionalLight = light;
                    return;
                }
            }
        }
    }
    public static float Clamp0360(float eulerAngles)
    {
        float result = eulerAngles - Mathf.CeilToInt(eulerAngles / 360f) * 360f;
        if (result < 0)
        {
            result += 360f;
        }
        return result;
    }
    private bool RoutineStoppedDayToNight
    {
        get
        {
            return stopDayToNightSkyColor;
        }
        set
        {
            stopDayToNightSkyColor = value;
            if (value)
            {
                StopCoroutine(dayToNightSkyColor);
                StartCoroutine(nightToDaySkyColor);
            }
        }
    }
    private bool RoutineStoppedNightToDay
    {
        get
        {
            return stopNightToDaySkyColor;
        }
        set
        {
            stopNightToDaySkyColor = value;
            if (value)
            {
                StopCoroutine(nightToDaySkyColor);
                StartCoroutine(dayToNightSkyColor);
            }
        }
    }
    // Update is called once per frame
    private void Update()
    {
        CalculatedRotationValue = Clamp0360(directionalLight.transform.localEulerAngles.x);
        //Grabs the rotation of the Directional Light. These rotations are the period of time that there is light, creating the Day cycle
        if (directionalLight.transform.localEulerAngles.x > 0 && directionalLight.transform.localEulerAngles.x < 180)
        {
            isNightTime = false;
            isDayTime = true;
            foreach (Light light in NightLights)
            {

                Debug.Log(intensity);
                if (intensity >= 0)
                {
                    intensity -= 0.4f * Time.deltaTime;
                }

                light.intensity = intensity;
            }

        }
        //Grabs the rotation of the Directional Light. These rotations are the period of time that there is darkness, creating the Night cycle
        else if (directionalLight.transform.localEulerAngles.x > 180 && directionalLight.transform.localEulerAngles.x < 360)
        {
            isDayTime = false;
            isNightTime = true;
        }

        #region DayNight rotation and period specifics
        directionalLight.transform.Rotate(speedFactor * DayTimeLength * Time.deltaTime, 0, 0);
        moon.transform.Rotate(speedFactor * NightTimeLength * Time.deltaTime, 0, 0);

        //rotates sun TODO: rotate moon
        if (isDayTime)
        {
            //directionalLight.gameObject.SetActive(true);
            //moon.gameObject.SetActive(false);
            RenderSettings.skybox = skybox;
            RenderSettings.sun = directionalLight;

            if (CalculatedRotationValue >= Mathf.Floor(0))
            {
                ResetCoroutines();
                foreach (ParticleSystem fireflies in Fireflies)
                {
                    ps = fireflies.GetComponent<ParticleSystem>();
                    var main = ps.main;
                    main.loop = false;
                }

                stars.gameObject.SetActive(false);
                var loop = stars.main;
                loop.loop = false;

                //StartCoroutine(LerpTimerDay(DayTimeLength));
            }
            if (CalculatedRotationValue > 80)
            {
                halfWayDay = true;
                halfWayNight = false;
            }
            if (halfWayDay && CalculatedRotationValue < 30)
            {
                lerpDaytime = true;
                lerpNighttime = false;
            }
            else if (halfWayDay == false)
            {
                lerpDaytime = false;
                lerpNighttime = true;
            }

        }

        if (isNightTime)
        {
            // directionalLight.gameObject.SetActive(false);
            // moon.gameObject.SetActive(true);
            //RenderSettings.skybox.SetFloat("_AthmospheerThickness", 4.999533f);
            //RenderSettings.skybox = nightSkyBox;

            RenderSettings.sun = moon;

            //  if (halfWayNight == false)
            //  {
            //      dayToNightSkyColor = LerpDayToNightSkyColor(3);
            //      //StartCoroutine(dayToNightSkyColor);
            //
            //  //   if (nightToDaySkyColor != null)
            //  //   {
            //  //       StopCoroutine(nightToDaySkyColor);
            //  //       nightToDaySkyColor = null;
            //  //   }
            //  }
            //  if (halfWayNight == true)
            //  {
            //      nightToDaySkyColor = LerpNightToDaySkyColor(3);
            //      //StartCoroutine(nightToDaySkyColor);
            //
            //     // if (dayToNightSkyColor != null)
            //     // {
            //     //     StopCoroutine(dayToNightSkyColor);
            //     //     dayToNightSkyColor = null;
            //     // }
            //  }

            if (CalculatedRotationValue >= Mathf.Floor(0))
            {
                foreach (ParticleSystem fireflies in Fireflies)
                {
                    ps = fireflies.GetComponent<ParticleSystem>();
                    ps.gameObject.SetActive(true);
                    var main = ps.main;
                    main.loop = true;
                }
                foreach (Light light in NightLights)
                {

                    Debug.Log(intensity);

                    if (intensity <= 4)
                    {
                        intensity += 0.2f * Time.deltaTime;
                    }

                    light.intensity = intensity;
                }

                stars.gameObject.SetActive(true);
                var loop = stars.main;
                loop.loop = true;

                //StartCoroutine(LerpNightToDaySkyColor(NightTimeLength));
                RoutineStoppedNightToDay = true;
                RoutineStoppedNightToDay = false;

            }
            if (CalculatedRotationValue < 300 && CalculatedRotationValue > 270)
            {
                halfWayDay = false;
                halfWayNight = true;
            }
            if (halfWayNight && CalculatedRotationValue >= Mathf.Floor(355))
            {
                RoutineStoppedDayToNight = true;
                RoutineStoppedDayToNight = false;
            }
        }
        #endregion

        if (lerpDaytime)
        {
            StartCoroutine(LerpDayToNight(3));
        }
        if (lerpNighttime)
        {
            StartCoroutine(LerpNightToDay(3));
        }

    }

    #region lerps for sunrise/sundawn
    IEnumerator LerpDayToNight(float duration)
    {
        float time = 0;

        while (time < duration)
        {
            LerpValue = Mathf.Lerp(LerpValue, nightSkyColor, (smoothnessFactor * speedFactor) * Time.deltaTime);
            RenderSettings.skybox.SetFloat("_AtmosphereThickness", LerpValue);
            sunIntensityValue = Mathf.Lerp(sunIntensityValue, 0f, (smoothnessFactor * speedFactor) * Time.deltaTime);
            directionalLight.intensity = sunIntensityValue;
            time += Time.deltaTime;
            yield return null;
        }
        //RenderSettings.skybox.SetFloat("_AtmosphereThickness", endValue);
    }
    IEnumerator LerpNightToDay(float duration)
    {
        float time = 0;
        while (time < duration)
        {
            LerpValue = Mathf.Lerp(LerpValue, daySkyColor, (smoothnessFactor * speedFactor) * Time.deltaTime);
            RenderSettings.skybox.SetFloat("_AtmosphereThickness", LerpValue);
            sunIntensityValue = Mathf.Lerp(sunIntensityValue, 1f, (smoothnessFactor * speedFactor) * Time.deltaTime);
            directionalLight.intensity = sunIntensityValue;
            time += Time.deltaTime;
            yield return null;
        }
        //RenderSettings.skybox.SetFloat("_AtmosphereThickness", endValue);
    }
    #endregion
    IEnumerator LerpDayToNightSkyColor(float duration)
    {
        float time = 0;

        while (time < duration)
        {
            skyColor = Color.Lerp(skyColor, nightSky, time);
            groundColor = Color.Lerp(groundColor, nightGround, time);
            sunSize = Mathf.Lerp(sunSize, nightSunSize, time);
            sunConvergence = Mathf.Lerp(sunConvergence, nightSunConvergence, time);
            exposure = Mathf.Lerp(exposure, nightExposure, time);

            RenderSettings.skybox.SetColor("_SkyTint", skyColor);
            RenderSettings.skybox.SetColor("_GroundTint", groundColor);
            RenderSettings.skybox.SetFloat("_SunSize", sunSize);
            RenderSettings.skybox.SetFloat("_SunSizeConvergence", sunConvergence);
            RenderSettings.skybox.SetFloat("_Exposure", exposure);
            time += (smoothnessFactor * speedFactor) * Time.deltaTime;

            yield return null;
        }
        if (sunSize <= Mathf.Floor(nightSunSize))
        {
            yield break;
        }
        // RoutineStoppedDayToNight = true;
    }
    IEnumerator LerpNightToDaySkyColor(float duration)
    {
        float time = 0;

        while (time < duration)
        {
            skyColor = Color.Lerp(skyColor, daySky, time);
            groundColor = Color.Lerp(groundColor, dayGround, time);
            sunSize = Mathf.Lerp(sunSize, daySunSize, time);
            sunConvergence = Mathf.Lerp(sunConvergence, daySunConvergence, time);
            exposure = Mathf.Lerp(exposure, dayExposure, time);

            RenderSettings.skybox.SetColor("_SkyTint", skyColor);
            RenderSettings.skybox.SetColor("_GroundTint", groundColor);
            RenderSettings.skybox.SetFloat("_SunSize", sunSize);
            RenderSettings.skybox.SetFloat("_SunSizeConvergence", sunConvergence);
            RenderSettings.skybox.SetFloat("_Exposure", exposure);
            time += (smoothnessFactor * speedFactor) * Time.deltaTime;
            yield return null;
        }

        if (sunSize >= Mathf.Ceil(daySunSize))
        {
            yield break;
        }
        // RoutineStoppedNightToDay = true;
    }
    public void ResetCoroutines()
    {
        nightToDaySkyColor = null;
        dayToNightSkyColor = null;
        nightToDaySkyColor = LerpNightToDaySkyColor(3);
        dayToNightSkyColor = LerpDayToNightSkyColor(3);
    }
}
