using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class CycleControllerOld : MonoBehaviour
{
    [SerializeField] private Light directionalLight;
    [SerializeField] private CyclePreset preset;

    [SerializeField, Range(0, 25)] private float timeOfDay;

    private void OnValidate()
    {
        if (directionalLight != null)
            return;

        if(RenderSettings.sun != null)
        {
            directionalLight = RenderSettings.sun;
        }
        else
        {
            Light[] lights = GameObject.FindObjectsOfType<Light>();
            foreach(Light light in lights)
            {
                if(light.type == LightType.Directional)
                {
                    directionalLight = light;
                    return;
                }
            }
        }
    }
    private void UpdateLighting(float timepercent)
    {
        RenderSettings.ambientLight = preset.AmbientColor.Evaluate(timepercent);
        RenderSettings.fogColor = preset.FogColor.Evaluate(timepercent);

        if(directionalLight != null)
        {
            directionalLight.color = preset.DirecitonalColor.Evaluate(timepercent);
        }
    }
    private void Update()
    {
        if(preset == null)
            return;

        if (Application.isPlaying)
        {
            timeOfDay += Time.deltaTime;
            timeOfDay %= 24;
            UpdateLighting(timeOfDay / 24f);
        }
        else
        {
            UpdateLighting(timeOfDay / 24f);
        }
    }
}
