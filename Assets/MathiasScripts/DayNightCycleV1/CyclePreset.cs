using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(fileName ="DayNight cycle preset", menuName = "Scriptables/DayNight cycle preset", order = 1)]
public class CyclePreset : ScriptableObject
{
    public Gradient AmbientColor;
    public Gradient DirecitonalColor;
    public Gradient FogColor;
}
