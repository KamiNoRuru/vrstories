using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System;
using System.IO;
using Assets;
using System.Threading.Tasks;

public class LocationManager : MonoBehaviour
{
    string pubIp = new System.Net.WebClient().DownloadString("https://api.ipify.org");
    public const string API_KEY = "at_FuHQMV88yU1iTaB7CS4OhS8H3oEST";
    public const string API_URL = "https://geo.ipify.org/api/v1?";
    string url;

    private void Start()
    {
        CheckLat();
    }
    public async void CheckLat()
    {
        float lattitude = (await GetGPS()).gps[0].lat;
        Debug.Log(lattitude);
    }
    private async Task<GPSInfo> GetGPS()
    {
        url = API_URL + $"apiKey={API_KEY}&ipAddress={pubIp}";
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync());
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();
        Debug.Log(jsonResponse);
        GPSInfo info = JsonUtility.FromJson<GPSInfo>(jsonResponse);

        return info;
    }
}
