﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Assets
{
    [Serializable]
    public class GPS
    {
        public float lat;
        public float lng;
    }
    [Serializable]
    public class GPSInfo
    {
        public float lat;
        public float lng;
        public List<GPS> gps;
    }
    }