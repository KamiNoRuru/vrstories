using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Net;
using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Assets;

public class EventManager : MonoBehaviour
{
    string pubIp = new System.Net.WebClient().DownloadString("https://api.ipify.org");

    public const string GEO_API_KEY = "at_FuHQMV88yU1iTaB7CS4OhS8H3oEST";
    public const string GEO_API_URL = "https://geo.ipify.org/api/v1?";

    private const string WEATHER_API_KEY = "4cfc94adbd4357b53febaa83ccf08810";

    string url; 

    public string lat;
    public string lng;

    public GameObject Snow;
    public GameObject Rain;

    public string jsontext;
    public string latString;
    public string lngString;

    private const float WEATHER_API_CHECK_MAXTIME = 10 * 60.0f; //10 minutes   
    private float _WeatherApiCheckCountdown = WEATHER_API_CHECK_MAXTIME;

    public string[] splitArray;

    // public LocationInfoClass gps = new LocationInfoClass();
    private void Start()
    {
        StartCoroutine(Upload());
    }

    IEnumerator Upload()
    {
        url = GEO_API_URL + $"apiKey={GEO_API_KEY}&ipAddress={pubIp}";

        using (UnityWebRequest location = UnityWebRequest.Get(url))

        {
            yield return location.SendWebRequest();
            Debug.Log(location.downloadHandler.text);
            jsontext = location.downloadHandler.text;

            // gps = JsonUtility.FromJson<LocationInfoClass>(jsontext);

            //LocationInfo.CreateFromJson(jsontext);
            //Debug.Log(LocationInfo.CreateFromJson(jsontext).lat);
           
            //parse json into elements
            splitArray = jsontext.Split(char.Parse(","));
           
            //get lat and long string from elements
            latString = splitArray[4];
            lngString = splitArray[5];

            //regex to get float values
            var matchLat = Regex.Match(latString, @"([-+]?[0-9]*\.?[0-9]+)");
            var matchLng = Regex.Match(lngString, @"([-+]?[0-9]*\.?[0-9]+)");
            
            //create strings from regex match values
            lat = matchLat.ToString();
            lng = matchLng.ToString();
            //OLD CheckSnowStatus();
            //OLD CheckRainStatus();
            CheckWeatherStatus();

        }
        Debug.Log("upload!");
    }
    #region Weather
   //OLD public async void CheckSnowStatus()
   //OLD {
   //OLD     bool snowing = (await GetWeather()).weather[0].main.Equals("Snow");
   //OLD 
   //OLD     if (snowing)
   //OLD         Snow.SetActive(true);
   //OLD     else
   //OLD         Snow.SetActive(false);
   //OLD }
   //OLD public async void CheckRainStatus()
   //OLD {
   //OLD     bool raining = (await GetWeather()).weather[0].main.Equals("Rain");
   //OLD 
   //OLD     if (raining)
   //OLD         Rain.SetActive(true);
   //OLD     else
   //OLD         Rain.SetActive(false);
   //OLD }
   public async void CheckWeatherStatus()
    {
        string weather = (await GetWeather()).weather[0].main;
        Debug.Log(weather);
     
        if (weather == "Rain")
            Rain.SetActive(true);
        else
            Rain.SetActive(false);

        if (weather == "Snow")
            Snow.SetActive(true);
        else Snow.SetActive(false);
    }

    private async Task<WeatherInfo> GetWeather()
    {
        //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("http://api.openweathermap.org/data/2.5/weather?id={0}&APPID={1}", CityId, WEATHER_API_KEY));

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("http://api.openweathermap.org/data/2.5/weather?lat={0}&lon={1}&APPID={2}", lat, lng, WEATHER_API_KEY));
        HttpWebResponse response = (HttpWebResponse)(await request.GetResponseAsync());
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string jsonResponse = reader.ReadToEnd();
        WeatherInfo info = JsonUtility.FromJson<WeatherInfo>(jsonResponse);
        Debug.Log(jsonResponse);
        return info;

    }
    #endregion //Weather
}