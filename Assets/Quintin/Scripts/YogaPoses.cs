using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Poses", menuName = "Scriptables/Yoga_Pose")]
public class YogaPoses : ScriptableObject
{
    public Sprite poseImage;

    public string poseName;
    public string effects;
}
