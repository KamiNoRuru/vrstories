using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayPose : MonoBehaviour
{
    public List<YogaPoses> poses;

    public Dictionary<string, YogaPoses> DPoses;
    [SerializeField] private int currentPose = 0;
    [SerializeField] private bool changePose = true;

    public TMP_Text poseName;
    public Image poseImage;

    // Start is called before the first frame update
    void Start()
    {
        AddPosesToDictionary();
        changePose = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.K)) {
            currentPose++;
            if (currentPose >= poses.Count) currentPose = 0;
            changePose = true;
        } else if (Input.GetKeyDown(KeyCode.J))
        {
            currentPose--;
            if (currentPose < 0) currentPose = poses.Count - 1;
            changePose = true;
        }

        if (changePose)
        {
            switch (currentPose)
            {
                case 0:
                    SetPose(DPoses["Warrior II"].poseName, DPoses["Warrior II"].poseImage);
                    return;

                case 1:
                    SetPose(DPoses["UpwardFacingDog"].poseName, DPoses["UpwardFacingDog"].poseImage);
                    return;

                default:
                    break;
            }
        }

    }

    private void SetPose(string poseName, Sprite poseImage)
    {
        this.poseName.text = poseName;
        this.poseImage.preserveAspect = true;
        this.poseImage.sprite = poseImage;
        changePose = false;
    }

    private void AddPosesToDictionary()
    {
        DPoses = new Dictionary<string, YogaPoses>()
        {
            { "UpwardFacingDog", poses[0] },
            { "Warrior II", poses[1] }
        };
    }
}
